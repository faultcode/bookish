import { useState, useEffect } from 'react';
import axios from 'axios';
import { useRemoteService } from './hook';
import RenderBooks from './BookList';

const BookListContainer = () => {
    const {data, loading, error} = useRemoteService([]);

    return <RenderBooks books={data} loading={loading} error={error}/>
}

export default BookListContainer;