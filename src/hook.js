import { useState, useEffect } from "react";

export const useRemoteService = () => {
    const [books, setBooks] = useState([]);
    const [loading, setLoading] = useState([]);
    const [error, setError] = useState([]);

    useEffect(() => {
        const fetchBooks = async() => {
          setError(false);
          setLoading(true);
      
          try {
            const res = await axios.get('http://localhost:8080/books');
            setBooks(res.data);
          } catch (e){
            setError(true);
          } finally {
            setLoading(false);
          }
        };
        fetchBooks();
      }, []);

      return {data, loading, error};
}